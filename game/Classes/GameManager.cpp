//
// Created by gorgo on 08.10.16.
//

#include "GameManager.h"



void GameManager::setScene(Node *scene) {
    m_scene = scene;
}

void GameManager::init() {
    log("init manager");


    m_tank.init();
    m_scene->addChild(m_tank.getNode());
    m_tank.getNode()->setPosition(Director::getInstance()->getVisibleSize() * 0.5f);

    initBullets();
    initMonsters();
    initBorders();

    m_blocksNode = Node::create();
    m_scene->addChild(m_blocksNode);
    resetBlocks();

    m_gameOverLabel = Label::createWithTTF("GAME OVER!\npress R to restart", "fonts/Marker Felt.ttf", 48);
    m_gameOverLabel->setVisible(false);
    m_gameOverLabel->setPosition(Director::getInstance()->getVisibleSize() * 0.5f);
    m_gameOverLabel->enableShadow();
    m_scene->addChild(m_gameOverLabel);

    Director::getInstance()->getScheduler()->scheduleUpdate<GameManager>(this, 0, false);
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = [this] (EventKeyboard::KeyCode code, Event *event) {
        this->keyPressed(code);
    };
    listener->onKeyReleased = [this] (EventKeyboard::KeyCode code, Event *event) {
        this->keyReleased(code);
    };
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, m_scene);
}

void GameManager::update(float dt) {
    if (m_keyPressed[EventKeyboard::KeyCode::KEY_LEFT_ARROW])
        m_tank.setRotationSpeed(TANK_MAX_ROTATION_SPEED);
    else if (m_keyPressed[EventKeyboard::KeyCode::KEY_RIGHT_ARROW])
        m_tank.setRotationSpeed(-TANK_MAX_ROTATION_SPEED);
    else
        m_tank.setRotationSpeed(0.0f);

    if (m_keyPressed[EventKeyboard::KeyCode::KEY_UP_ARROW])
        m_tank.setLinearSpeed(TANK_MAX_LINEAR_SPEED);
    else if (m_keyPressed[EventKeyboard::KeyCode::KEY_DOWN_ARROW])
        m_tank.setLinearSpeed(-TANK_MAX_LINEAR_SPEED);
    else
        m_tank.setLinearSpeed(0.0f);

    if (m_gameIsOver) return;

    m_tank.update(dt);

    //update bullets
    for (size_t i = 0; i < BULLET_MAX; ++i) {
        Bullet *bullet = &m_bullets[i];
        if (bullet->active) {
            bullet->sprite->setPosition(bullet->sprite->getPosition() + bullet->speed * dt);
            bullet->timeLeft -= dt;
            if (bullet->timeLeft <= 0) {
                bullet->sprite->setVisible(false);
                bullet->active = false;
            }
        }
    }

    //update monsters
    //spawn
    m_monsterSpawnTimeout -= dt;
    if (m_monsterSpawnTimeout <= 0) {
        for (size_t i = 0; i < MONSTER_MAX; ++i) {
            if (!m_monsters[i].isActive()) {
                m_monsters[i].reset();
                m_monsterSpawnTimeout = MONSTER_SPAWN_TIME;
                break;
            }
        }
    }

    // movement
    for (size_t i = 0; i < MONSTER_MAX; ++i) {
        if (m_monsters[i].isActive()) {
            float speed = m_monsters[i].getSpeed();
            Vec2 pos = m_monsters[i].getNode()->getPosition();
            Vec2 direction = m_tank.getNode()->getPosition() - pos;
            direction.normalize();
            m_monsters[i].getNode()->setPosition(pos + speed * direction * dt);
            m_monsters[i].getNode()->setRotation(270-MATH_RAD_TO_DEG(direction.getAngle()));
        }
    }

    //detect monster-tank collision
    for (size_t i = 0; i < MONSTER_MAX; ++i) {

        if (m_monsters[i].isActive() && isColliding(&m_monsters[i], &m_tank)) {
            m_tank.applyDamage(m_monsters[i].getDamage());
            m_monsters[i].setActive(false);
            if (m_tank.getHealth() <= 0)
                gameOver();
        }

    }

    //detect bullet-monster collision
    for (size_t b = 0; b < BULLET_MAX; ++b) {
        for (size_t m = 0; m < MONSTER_MAX; ++m) {
            if (m_bullets[b].active && m_monsters[m].isActive()) {
                pair<Vec2, float> mcData = m_monsters[m].getCollisionData();
                if ((m_bullets[b].sprite->getPosition() - mcData.first).length() < mcData.second) {
                    m_bullets[b].active = false;
                    m_bullets[b].sprite->setVisible(false);
                    m_monsters[m].applyDamage(m_bullets[b].damage);
                    if (m_monsters[m].getHealth() <= 0)
                        m_monsters[m].setActive(false);
                }
            }

        }
    }

    //resolve collisions both monsters and tank with static bodies
    for (size_t i = 0; i < MONSTER_MAX; ++i) {
        m_monsters[i].getNode()->setPosition(resolveStaticCollision(
                m_monsters[i].getNode()->getPosition(), BLOCK_C_BIT,
                m_monsters[i].getCollisionData().second)); // should collide only with blocks
    }
    m_tank.getNode()->setPosition(resolveStaticCollision(
            m_tank.getNode()->getPosition(), BORDER_C_BIT | BLOCK_C_BIT,
            m_tank.getCollisionData().second)); // should collide both with borders and blocks

    //bullet absorbtion
    for (size_t c = 0; c < STATIC_COLLIDERS; ++c) {
        for (size_t b = 0; b < BULLET_MAX; ++b) {
            if (m_bullets[b].active
                && m_staticColliders[c].first.containsPoint(
                    m_bullets[b].sprite->getPosition())) {

                m_bullets[b].active = false;
                m_bullets[b].sprite->setVisible(false);
                break;
            }
        }
    }
}

void GameManager::keyPressed(EventKeyboard::KeyCode code) {
    if (code == EventKeyboard::KeyCode::KEY_X)
        shoot();
    else if (code == EventKeyboard::KeyCode::KEY_Q)
        m_tank.switchWeapon(-1);
    else if (code == EventKeyboard::KeyCode::KEY_W)
        m_tank.switchWeapon(1);
    else if (code == EventKeyboard::KeyCode::KEY_R)
        restart();
    else if (code == EventKeyboard::KeyCode::KEY_LEFT_ARROW
             || code == EventKeyboard::KeyCode::KEY_RIGHT_ARROW
             || code == EventKeyboard::KeyCode::KEY_UP_ARROW
             || code == EventKeyboard::KeyCode::KEY_DOWN_ARROW)
    {
        m_keyPressed[code] = true;
    }

}

void GameManager::keyReleased(EventKeyboard::KeyCode code) {
    if (code == EventKeyboard::KeyCode::KEY_LEFT_ARROW
            || code == EventKeyboard::KeyCode::KEY_RIGHT_ARROW
            || code == EventKeyboard::KeyCode::KEY_UP_ARROW
            || code == EventKeyboard::KeyCode::KEY_DOWN_ARROW)
        m_keyPressed[code] = false;

}

void GameManager::shoot() {
    //find free bullet, take the oldest otherwise
    size_t i = 0, bulletId = i;
    float minTimeLeft = m_bullets[i].timeLeft;
    for (; i < BULLET_MAX; ++i) {
        if (!m_bullets[i].active) {
            bulletId = i;
            break;
        }
        if (m_bullets[i].timeLeft < minTimeLeft) {
            minTimeLeft = m_bullets[i].timeLeft;
            bulletId = i;
        }
    }
    Bullet *bullet = &m_bullets[bulletId];
    bullet->timeLeft = BULLET_TIME;
    bullet->damage = m_tank.getDamage();
    bullet->sprite->setVisible(true);
    bullet->active = true;
    bullet->sprite->setPosition(m_tank.getNode()->getPosition());
    bullet->speed = m_tank.getDirection() * BULLET_SPEED;
    log("speed = %f, %f", bullet->speed.x, bullet->speed.y);
}

void GameManager::initBullets() {
    m_bulletNode = Node::create();
    m_scene->addChild(m_bulletNode);
    for (size_t i = 0; i < BULLET_MAX; ++i) {
        m_bullets[i].sprite = Sprite::create("res/bullet.png");
        m_bulletNode->addChild(m_bullets[i].sprite);
        m_bullets[i].sprite->setVisible(false);
        m_bullets[i].sprite->retain();
    }
}

void GameManager::initMonsters() {
    m_monstersNode = Node::create();
    m_scene->addChild(m_monstersNode);
    m_monsterSpawnTimeout = MONSTER_SPAWN_TIME;
    for (size_t i = 0; i < MONSTER_MAX; ++i) {
        m_monsters[i].init();
        m_monstersNode->addChild(m_monsters[i].getNode());

        m_monsters[i].setActive(false);
    }
}

bool GameManager::isColliding(ICollidable *one, ICollidable *two) {
    pair<Vec2, float> dataOne = one->getCollisionData();
    pair<Vec2, float> dataTwo = two->getCollisionData();
    return (dataTwo.first - dataOne.first).length() - (dataOne.second + dataTwo.second) < 0;
}

void GameManager::gameOver() {
    m_gameIsOver = true;
    m_gameOverLabel->setVisible(true);
}

void GameManager::restart() {
    resetBlocks();
    m_tank.getNode()->setPosition(Director::getInstance()->getVisibleSize() * 0.5f);
    m_tank.getNode()->setRotation(0);
    m_tank.reset();
    for (size_t i = 0; i < MONSTER_MAX; ++i) {
        m_monsters[i].setActive(false);
    }
    m_monsterSpawnTimeout = MONSTER_SPAWN_TIME;
    m_gameIsOver = false;
    m_gameOverLabel->setVisible(false);
}

Vec2 GameManager::resolveStaticCollision(Vec2 pos, short mask, float expand = 0.0f) {
    //simple aa rect alignment
    for (size_t i = 0; i < STATIC_COLLIDERS; ++i) {
        Rect r = Rect(m_staticColliders[i].first.origin - Vec2(expand,expand),
                      m_staticColliders[i].first.size + (Size)Vec2(expand*2, expand*2));
        if (r.containsPoint(pos) && (m_staticColliders[i].second & mask) > 0) {
            //need some resolving
            //find min distance to the border and project to it
            pair<float, Vec2> variants[4];

            variants[0].first = fabs(pos.x - r.getMinX());
            variants[0].second = Vec2(r.getMinX(), pos.y);
            variants[1].first = fabs(pos.x - r.getMaxX());
            variants[1].second = Vec2(r.getMaxX(), pos.y);
            variants[2].first = fabs(pos.y - r.getMinY());
            variants[2].second = Vec2(pos.x, r.getMinY());
            variants[3].first = fabs(pos.y - r.getMaxY());
            variants[3].second = Vec2(pos.x, r.getMaxY());
            float minDist = variants[0].first;
            pos = variants[0].second;
            for (size_t v = 1; v < 4; ++v) {
                if (variants[v].first < minDist) {
                    pos = variants[v].second;
                    minDist = variants[v].first;
                }
            }

        }
    }
    return pos;
}

void GameManager::initBorders() {
    Vec2 size = Director::getInstance()->getVisibleSize();
    const float t = 50.0f;
    m_staticColliders[0] = pair<Rect, short>(Rect(-t, -t, t, t * 2 + size.y), BORDER_C_BIT);
    m_staticColliders[1] = pair<Rect, short>(Rect(size.x, -t, t, t*2 + size.y), BORDER_C_BIT);
    m_staticColliders[2] = pair<Rect, short>(Rect(-t, -t, t*2 + size.x, t), BORDER_C_BIT);
    m_staticColliders[3] = pair<Rect, short>(Rect(-t, size.y, t*2 + size.x, t), BORDER_C_BIT);
}

void GameManager::resetBlocks() {
    m_blocksNode->removeAllChildren();
    Vec2 size = Director::getInstance()->getVisibleSize();
    const size_t borderMax = 3;
    for (size_t i = borderMax + 1; i < STATIC_COLLIDERS; ++i) {
        Sprite *s = Sprite::create("res/box.png");
        m_blocksNode->addChild(s);
        s->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        Vec2 sSize = s->getContentSize();
        log("sprite size=  %f, %f", sSize.x, sSize.y);
        m_staticColliders[i] = pair<Rect, short>(Rect(
                RandomHelper::random_real<float>(0.0f, size.x - sSize.x),
                RandomHelper::random_real<float>(0.0f, size.y - sSize.y),
                sSize.x, sSize.y), BLOCK_C_BIT);
        s->setPosition(m_staticColliders[i].first.origin);
    }
}






