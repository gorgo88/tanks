//
// Created by gorgo on 10.10.16.
//

#include "Actor.h"

void Actor::applyDamage(float damage) {
    m_health -= damage * getArmor();

    getMainSprite()->setColor(Color3B::RED);
    Action *action = TintTo::create(0.2f, Color3B::WHITE);
    getMainSprite()->runAction(action);
}

