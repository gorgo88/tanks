//
// Created by gorgo on 10.10.16.
//

#ifndef MYGAME_TANK_H
#define MYGAME_TANK_H

#include <cocos2d.h>
#include <ICollidable.h>
#include <Actor.h>

using namespace cocos2d;
using namespace std;

static const size_t WEAPON_TYPE_COUNT = 3;

class Tank : public Actor, public ICollidable {

    struct WeaponType {
        string path;
        float damage;
    };

    const float COLLISION_RADIUS = 30.0f;
    const float HEALTH_MAX = 150.0f;
    const float ARMOR = 0.8f;

    const WeaponType WEAPON_TYPES[WEAPON_TYPE_COUNT] = {
           {"res/top_0.png", 80.0f},
           {"res/top_1.png", 105.0f},
           {"res/top_2.png", 150.0f},
    };

    Node *m_weaponNode;
    size_t m_weaponTypeId;

    float m_rotSpeed = 0.0f;
    float m_linearSpeed = 0.0f;

public:

    void init();

    void reset();

    Vec2 getDirection();

    void switchWeapon(int inc);

    void update(float dt);

    void setRotationSpeed(float speed);

    void setLinearSpeed(float speed);

    virtual pair<Vec2, float> getCollisionData() override;

    virtual float getDamage() override;

    virtual float getArmor() override;

    virtual Sprite *getMainSprite() override;

private:

    void updateWeapon();
};


#endif //MYGAME_TANK_H
