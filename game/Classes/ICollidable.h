//
// Created by gorgo on 10.10.16.
//

#ifndef MYGAME_ICOLLIDABLE_H
#define MYGAME_ICOLLIDABLE_H

#include <cocos2d.h>

using namespace cocos2d;
using namespace std;

class ICollidable {
public:
    // position and radius
    virtual pair<Vec2, float> getCollisionData() = 0;
};


#endif //MYGAME_ICOLLIDABLE_H
