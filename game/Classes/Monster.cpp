//
// Created by gorgo on 10.10.16.
//

#include "Monster.h"

void Monster::reset() {
    Vec2 size = Director::getInstance()->getVisibleSize();
    float radius = size.length() * 0.5f;
    Vec2 pos = size * 0.5f + Vec2::forAngle(RandomHelper::random_real<float>(0,(float)M_PI*2))*radius;
    m_node->setPosition(pos);
    m_typeId = RandomHelper::random_int<size_t>(0, MONSTER_TYPE_COUNT - 1);


    m_sprite->setTexture(MONSTER_TYPES[m_typeId].path);
    m_health = MONSTER_TYPES[m_typeId].maxHealth;

    setActive(true);

}

void Monster::init() {
    m_node = Node::create();
    m_sprite = Sprite::create();
    m_node->addChild(m_sprite);
}

pair<Vec2, float> Monster::getCollisionData() {
    return pair<Vec2, float>(m_node->getPosition(), COLLISION_RADIUS);
}

float Monster::getArmor() {
    return MONSTER_TYPES[m_typeId].armor;
}

float Monster::getDamage() {
    return MONSTER_TYPES[m_typeId].damage;
}

Sprite *Monster::getMainSprite() {
    return m_sprite;
}

