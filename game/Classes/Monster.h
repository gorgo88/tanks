//
// Created by gorgo on 10.10.16.
//

#ifndef MYGAME_MONSTER_H
#define MYGAME_MONSTER_H

#include <cocos2d.h>
#include <ICollidable.h>
#include <Actor.h>

using namespace cocos2d;
using namespace std;

static const size_t MONSTER_TYPE_COUNT = 3;

class Monster : public Actor, public ICollidable {
    struct MonsterType {
        string path;
        float maxHealth;
        float damage;
        float armor;
        float speed;
    };

    const float COLLISION_RADIUS = 30.0f;

    const MonsterType MONSTER_TYPES[MONSTER_TYPE_COUNT] = {
          {"res/monster_0.png", 100.0f, 20.0f, 0.8f, 30.0f},
          {"res/monster_1.png", 150.0f, 40.0f, 0.7f, 50.0f},
          {"res/monster_2.png", 200.0f, 60.0f, 0.5f, 70.0f},
    };

    size_t m_typeId;
    Sprite *m_sprite;
    bool m_active;

public:
    void init();
    void reset();

    float getSpeed() {return MONSTER_TYPES[m_typeId].speed;}


    bool isActive() {return m_active;}

    void setActive(bool active) {
        m_active = active;
        m_node->setVisible(active);
    }

    virtual float getDamage() override;

    virtual float getArmor() override;

    virtual pair<Vec2, float> getCollisionData() override;

    virtual Sprite *getMainSprite() override;
};

#endif //MYGAME_MONSTER_H
