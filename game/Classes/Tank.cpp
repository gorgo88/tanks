//
// Created by gorgo on 10.10.16.
//

#include "Tank.h"

void Tank::init() {
    m_node = Node::create();
    Sprite *base = Sprite::create("res/tank.png");
    m_node->addChild(base);

    m_weaponNode = Node::create();
    reset();
    m_node->addChild(m_weaponNode);
}

void Tank::reset() {
    m_health = HEALTH_MAX;
    m_weaponTypeId = 0;
    updateWeapon();
}


void Tank::switchWeapon(int inc) {
    m_weaponTypeId = (size_t)(((int)(m_weaponTypeId + WEAPON_TYPE_COUNT) + inc) % (int)WEAPON_TYPE_COUNT); // clamp
    updateWeapon();
}


void Tank::update(float dt) {
    //log("rotation %f", m_rotSpeed);
    m_node->setRotation(m_node->getRotation() - (m_rotSpeed * dt));
    m_node->setPosition(m_node->getPosition() + getDirection()*m_linearSpeed * dt);
}

void Tank::setRotationSpeed(float speed) {
    m_rotSpeed = speed;
}

void Tank::setLinearSpeed(float speed) {
    m_linearSpeed = speed;
}

void Tank::updateWeapon() {
    m_weaponNode->removeAllChildren();
    m_weaponNode->addChild(Sprite::create(WEAPON_TYPES[m_weaponTypeId].path));
}


Vec2 Tank::getDirection() {
    return Vec2::forAngle(CC_DEGREES_TO_RADIANS(90 - m_node->getRotation()));
}

pair<Vec2, float> Tank::getCollisionData() {
    return pair<Vec2, float>(m_node->getPosition(), COLLISION_RADIUS);
}

float Tank::getDamage() {
    return WEAPON_TYPES[m_weaponTypeId].damage;
}

float Tank::getArmor() {
    return ARMOR;
}

Sprite *Tank::getMainSprite() {
    return (Sprite*)m_node->getChildren().at(0);
}


