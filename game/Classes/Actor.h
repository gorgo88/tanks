//
// Created by gorgo on 10.10.16.
//

#ifndef MYGAME_ACTOR_H
#define MYGAME_ACTOR_H

#include <cocos2d.h>

using namespace cocos2d;

class Actor {
protected:
    float m_health;
    Node *m_node;
public:
    Node *getNode(){return m_node;}

    float getHealth() {return m_health;}
    void applyDamage(float damage);

    virtual float getDamage() = 0;
    virtual float getArmor() = 0;
    virtual Sprite *getMainSprite() = 0;
};


#endif //MYGAME_ACTOR_H
