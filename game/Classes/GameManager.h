//
// Created by gorgo on 08.10.16.
//

#ifndef MYGAME_GAMEMANAGER_H
#define MYGAME_GAMEMANAGER_H

#include <cocos2d.h>
#include <Tank.h>
#include <Monster.h>

using namespace cocos2d;
using namespace std;


static const float TANK_MAX_ROTATION_SPEED  = 200.0f;
static const float TANK_MAX_LINEAR_SPEED    = 100.0f;
static const size_t BULLET_MAX              = 5;
static const float BULLET_SPEED             = 500.0f;
static const float BULLET_TIME              = 1.0f;
static const float MONSTER_SPAWN_TIME       = 3.0f;
static const size_t MONSTER_MAX             = 10;
static const size_t STATIC_COLLIDERS        = 7; // include borders


struct Bullet {
    float timeLeft;
    float damage;
    bool active;
    Vec2 speed;
    Sprite *sprite;
};

class GameManager {
    const short BORDER_C_BIT = 1<<0;
    const short BLOCK_C_BIT = 1<<1;

    Node *m_scene;
    Tank m_tank;
    Node *m_bulletNode;
    Monster m_monsters[MONSTER_MAX];
    float m_monsterSpawnTimeout;
    Node *m_monstersNode;
    bool m_gameIsOver = false;
    Label* m_gameOverLabel;
    Node *m_blocksNode;

    //rect and collision mask
    pair<Rect, short> m_staticColliders[STATIC_COLLIDERS];
    Bullet m_bullets[BULLET_MAX] = {{0.0f, 0.0f, false, Vec2(), nullptr}};

    map<EventKeyboard::KeyCode, bool> m_keyPressed = {
            {EventKeyboard::KeyCode::KEY_LEFT_ARROW, false},
            {EventKeyboard::KeyCode::KEY_RIGHT_ARROW, false},
            {EventKeyboard::KeyCode::KEY_UP_ARROW, false},
            {EventKeyboard::KeyCode::KEY_DOWN_ARROW, false}
    };
public:

    void init();
    void update(float dt);
    void setScene(Node *scene);

private:

    void keyPressed(EventKeyboard::KeyCode code);

    void keyReleased(EventKeyboard::KeyCode code);

    void shoot();

    void initBullets();

    void initMonsters();

    bool isColliding(ICollidable *one, ICollidable *two);

    void gameOver();

    void restart();

    Vec2 resolveStaticCollision(Vec2 pos, short mask, float expand);

    void initBorders();

    void resetBlocks();
};


#endif //MYGAME_GAMEMANAGER_H
