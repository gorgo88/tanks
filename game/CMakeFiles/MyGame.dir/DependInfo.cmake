# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gorgo/projects/tanks/game/Classes/Actor.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/Actor.cpp.o"
  "/home/gorgo/projects/tanks/game/Classes/AppDelegate.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/AppDelegate.cpp.o"
  "/home/gorgo/projects/tanks/game/Classes/GameManager.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/GameManager.cpp.o"
  "/home/gorgo/projects/tanks/game/Classes/HelloWorldScene.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/HelloWorldScene.cpp.o"
  "/home/gorgo/projects/tanks/game/Classes/Monster.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/Monster.cpp.o"
  "/home/gorgo/projects/tanks/game/Classes/Tank.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/Classes/Tank.cpp.o"
  "/home/gorgo/projects/tanks/game/proj.linux/main.cpp" "/home/gorgo/projects/tanks/game/CMakeFiles/MyGame.dir/proj.linux/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gorgo/projects/tanks/game/cocos2d/cocos/CMakeFiles/cocos2d.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/cocos/CMakeFiles/cocos2dInternal.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/external/unzip/CMakeFiles/unzip.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/external/tinyxml2/CMakeFiles/tinyxml2.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/external/xxhash/CMakeFiles/xxhash.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/external/bullet/CMakeFiles/bullet.dir/DependInfo.cmake"
  "/home/gorgo/projects/tanks/game/cocos2d/external/recast/CMakeFiles/recast.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/GLFW"
  "/usr/include/GLFW"
  "cocos2d/cocos"
  "cocos2d/cocos/platform"
  "cocos2d/cocos/audio/include"
  "Classes"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
